package azp;

import java.sql.*;
import java.util.ArrayList;

public class DbTool {
    public static void main(String[] args) {
    	String [] alumnos = new String[DbTool.getUserNames().size()];
    	for (int i=0; i<DbTool.getUserNames().size(); i++){
    		alumnos[i] = DbTool.getUserNames().get(i);
    	}
    	for (String s: alumnos) System.out.print(s);
    }
    public DbTool() {
    }
    public DbTool(String s) {
        s = dbConStr;
    }
    public static Connection getMyConnection() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            dbConnection = java.sql.DriverManager.getConnection(dbConStr, dbUserStr, dbPassStr);
        } catch (Exception e) {
            System.out.println("[ERROR] - DbTool - getMyConnection: " + e);
        }
        return dbConnection;
    }
    public static ArrayList<String> getUsers() {
        userList = new ArrayList<String>();
        try {
            myPreparedStatement = getMyConnection().prepareStatement(dbSelectUsers,
                    ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
            myResultSet = myPreparedStatement.executeQuery();
            while (myResultSet.next()) {
                userList.add(myResultSet.getString(1) + "\t"
                        + myResultSet.getString(2) + "\t"
                        + myResultSet.getString(3) + "\t"
                        + myResultSet.getString(4) + "\t"
//                        + myResultSet.getString(5) + "\t"
                        + myResultSet.getString(6) + "\t"
                        + myResultSet.getString(7) + "\t"
                        + "\n"
                );
            }
        } catch (Exception e) {
            System.out.println("[ERROR] - DbTool - getUsers: " + e);
        }
        cleanUp();
        return userList;
    }
    public static ArrayList<String> getUserNames() {
        userList = new ArrayList<String>();
        try {
            myPreparedStatement = getMyConnection().prepareStatement(dbSelectUsers,
                    ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
            myResultSet = myPreparedStatement.executeQuery();
            while (myResultSet.next()) {
                userList.add(myResultSet.getString(2) + "\t"
                        + "\n"
                );
            }
        } catch (Exception e) {
            System.out.println("[ERROR] - DbTool - getUsers: " + e);
        }
        cleanUp();
        return userList;
    }
    public static ArrayList<String> getTableContent(String tableName) {
        tableContent = new ArrayList<String>();
        try {
            myPreparedStatement = getMyConnection().prepareStatement("SELECT * FROM " + dbName + "." + tableName,
                    ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
            myResultSet = myPreparedStatement.executeQuery();

            myMetaData = dbConnection.getMetaData();
            myMetaData.getColumns(dbName, dbName, tableName, null);
            columnCount = getColumnCount(tableName);
            while (myResultSet.next()) {
                for (int i=1; i<=columnCount; i++) {
                    tableContent.add(myResultSet.getString(i) + "\t");
                }
                tableContent.add("<br>");
            }
        } catch (Exception e) {
            System.out.println("[ERROR] - DbTool - getTableContent: " + e.getMessage());
        }
        cleanUp();
        return tableContent;
    }
    public static Boolean getValidatedUser(String usuario, String contra) {
        try {
            myPreparedStatement = getMyConnection().prepareStatement(dbSelectUsersWhere,
                    ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
            myPreparedStatement.setString(1, usuario);
            myPreparedStatement.setString(2, contra);
            myResultSet = myPreparedStatement.executeQuery();
            validatedUser = myResultSet.absolute(1);
        } catch (Exception e) {
            System.out.println("[ERROR] - DbTool - getValidatedUser: " + e);
        }
        cleanUp();
        return validatedUser;
    }
    public static int getColumnCount(String tableName) {
        columnCount = 0;
        try {
            myMetaData = getMyConnection().getMetaData();
            myMetaDataResultSet = myMetaData.getColumns(dbName, dbName, tableName, null);
            while (myMetaDataResultSet.next()) {
                columnCount++;
            }
        } catch (Exception e) {
            System.out.println("[ERROR] - DbTool - getColumnCount: " + e);
        } finally {
            try {
                myMetaDataResultSet.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return columnCount;
    }

    public static int getColumnCount(String dbName, String tableName) {
        try {
            myMetaData = getMyConnection().getMetaData();
            myMetaDataResultSet = myMetaData.getColumns(dbName, dbName, tableName, null);
            while (myMetaDataResultSet.next()) {
                columnCount++;
            }
        } catch (Exception e) {
            System.out.println("[ERROR] - DbTool - getColumnCount: " + e);
        }
        return columnCount;
    }
    public static void cleanUp() {
        try {
            if (myResultSet != null) myResultSet.close();
            if (myMetaDataResultSet != null) myMetaDataResultSet.close();
            if (dbConnection != null) dbConnection.close();
        } catch (Exception e) {
            System.out.println("[ERROR] - DbTool - cleanUp: " + e);
        }

    }

    private static Connection dbConnection;
    private static DatabaseMetaData myMetaData;
    private static Boolean validatedUser;
    private static int columnCount;
    private static ArrayList<String> userList, tableContent;
    private static ResultSet myResultSet, myMetaDataResultSet;
    private static PreparedStatement myPreparedStatement;
    private static final String tableName = "USERS";
    // private static String dbName = "proyectojsp";
    private static final String dbName = "api";
    private static final String dbServer = "192.168.1.200";
    private static final String dbPort = "3306";
    private static final String dbConStr = "jdbc:mysql://" + dbServer + ":" + dbPort + "/" + dbName;
    // private static final String dbUserStr = "jsp_user";
    // private static final String dbPassStr = "jsp_user";
    private static final String dbUserStr = "root";
    private static final String dbPassStr = "w0rdp3Ss";
    private static final String dbSelectUsers = "SELECT * FROM " + tableName;
    private static final String dbSelectUsersWhere = "SELECT username FROM proyectojsp.USERS WHERE username=? AND passphrase=?";
}
