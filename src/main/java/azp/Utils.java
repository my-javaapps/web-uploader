package azp;

public class Utils {
//    public static void main(String args[]) {
//        Utils2 myUtils = new Utils2();
//        myUtils.logInfo("main");
//    }
    public static String getHomeDirectory() {
        return System.getProperty("user.home");
    }
    public static java.util.Date date = new java.util.Date();
    String className = String.valueOf(Utils.class);


    public static void logInfo(String s) {
        System.out.println(date + " [INFO]: " + s);
    }

    public static void logError(String s) {
        System.out.println(date + " [ERROR]: " + s);
    }
}
