package azp.web;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static azp.Utils.logError;

public class Utils {
    public static void getLogin(HttpServletRequest request, HttpServletResponse response) {
        try {
            RequestDispatcher myDispatcher = request.getRequestDispatcher("/Login.jsp");
            myDispatcher.forward(request, response);
        } catch (Exception e) {
            logError("Utils - getLogin(): " + e.getMessage());
        }
    }

    public static void getUploader(HttpServletRequest request, HttpServletResponse response) {
        try {
            RequestDispatcher myDispatcher = request.getRequestDispatcher("/Uploader.jsp");
            myDispatcher.forward(request, response);
        } catch (Exception e) {
            logError("Utils - getUploader(): " + e.getMessage());
        }
    }
}
