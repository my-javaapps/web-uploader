package azp.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.nio.file.Paths;
import java.nio.file.Files;

import static azp.Utils.logInfo;

@WebServlet(name = "FileUploadServlet", urlPatterns = { "/fileuploadservlet" })
@MultipartConfig(
		fileSizeThreshold = 1024 * 1024,	// 1MB
		maxFileSize = -1,		// 1GB
		maxRequestSize = -1		// 1GB
		)
public class FileUploadServlet extends HttpServlet {
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Files.createDirectories(Paths.get(dstPath));
		Part filePart = request.getPart("file");
		String fileName = filePart.getSubmittedFileName();
		for (Part part: request.getParts()) {
			logInfo(part.getSubmittedFileName());
			part.write(dstPath + fileName);
		}
		response.getWriter().print("The file was uploaded successfully.");
	}
	private static final String dstPath = "/tmp/Uploads/";
}
