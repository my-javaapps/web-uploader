package azp.web;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static azp.Utils.logInfo;

@WebServlet("/Uploader")
public class Uploader extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Leemos el parámetro "loginStr" del formulario
        // si no existe, lo seteamos a null
        String loginStr;
        if (request.getParameterNames().hasMoreElements()) {
            loginStr = request.getParameter("loginStr");
        } else {
            loginStr = null;
        }

        logInfo("Uploader - doGet - loginStr value: " + loginStr);

        // Si el loginStr es null, pedimos login de nuevo
        if (loginStr == null) {
            Utils.getLogin(request, response);
        // Si el loginStr es igual a passStr, dejamos pasar
        } else if (loginStr.equals(passStr)) {
            Utils.getUploader(request, response);
        // Si el loginStr no es igual a passStr, pedimos login
        } else {
            Utils.getLogin(request, response);
        }
    }

    private static String passStr = "P@ssw0rd";

}
