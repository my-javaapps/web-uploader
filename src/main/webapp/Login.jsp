<%@ page language="java"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="azp.products.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>

<!DOCTYPE html>
<html lang="es">
<html>
<head>
	<meta http-equiv="content-type" content="text/html" charset="UTF-8"/>
  	<link rel="icon" type="image/x-icon" href="images/favicon.ico">
	<link rel="stylesheet" href="Styles.css">
	<script src="https://kit.fontawesome.com/14c5c7ef75.js" crossorigin="anonymous"></script>
	<c:set var="title" value="Login"></c:set>
	<title><c:out value="${title}"></c:out></title>

</head>

<body>
<div class="center" id="main-thing">
	<h1>
		<c:out value="${title}"></c:out>
		<i class="fas fa-cloud"></i>
	</h1>
	<form name="form1" method="GET" action="Uploader">
	    <input type="password" id="pass" name="loginStr" minlength="8" required />
	    <input id="center-sub-element" type="submit" value="Entrar" />
	</form>
</div>
</body>
</html>