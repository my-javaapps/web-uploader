<%@ page language="java"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="azp.products.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>

<!DOCTYPE html>
<html lang="es">
<html>
<head>
	<meta http-equiv="content-type" content="text/html" charset="UTF-8"/>
  	<link rel="icon" type="image/x-icon" href="images/favicon.ico">
	<link rel="stylesheet" href="Styles.css">
	<script src="https://kit.fontawesome.com/14c5c7ef75.js" crossorigin="anonymous"></script>
	<c:set var="title" value="Uploader"></c:set>
	<title><c:out value="${title}"></c:out></title>
    <script type="text/javascript" src="plupload/js/plupload.full.min.js"></script>

</head>

<body>
<div class="center" id="main-thing">
	<h1>
		<c:out value="${title}"></c:out>
		<i class="fas fa-cloud"></i>
	</h1>
    <br />
    <button id="pickfiles" class="edit">Select file</button>
    <button id="uploadfiles" class="submit">Upload</button>

    <div class="center-sub-element" id="filelist"></div>

    <input type="hidden" id="container_div">

    <pre id="console"></pre>
    <script type="text/javascript">
        // Custom example logic
        var uploader = new plupload.Uploader({
            runtimes : 'html5',
            browse_button : 'pickfiles', // you can pass an id...
            container_div: document.getElementById('container_div'), // ... or DOM Element itself
            url : '../UploadAction',//upload.php
            chunk_size : '64mb',
            method:'POST',
            flash_swf_url : 'plupload/js/Moxie.swf',
            silverlight_xap_url : 'plupload/js/Moxie.xap',

            filters : {
                max_file_size : '100gb',
                mime_types: [
                    {title : "Image files", extensions : "jpg,gif,png"},
                    {title : "Video files", extensions : "mpeg,mp4,avi,mkv"},
                    {title : "Switch files", extensions : "nsp,xci"},
                    {title : "Zip files", extensions : "zip,txt,vmdk,dmg,iso,tar,tgz"}
                ]
            },
            init: {
                PostInit: function() {
                    document.getElementById('filelist').innerHTML = '';
                    document.getElementById('uploadfiles').onclick = function() {
                        uploader.start();
                        return false;
                    };
                },
                FilesAdded: function(up, files) {
                    plupload.each(files, function(file) {
                        document.getElementById('filelist').innerHTML += '<div class="center-sub-element" id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
                    });
                },
                UploadProgress: function(up, file) {
                    document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
                },
                Error: function(up, err) {
                    document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
                }
            }
        });
        uploader.init();
    </script>
</div>
</body>
</html>